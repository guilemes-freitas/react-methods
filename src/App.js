import { Component } from 'react';
import Fruit from './components/Fruit'
import './App.css'

class App extends Component {
  fruits = [
    { name: "banana", color: "yellow", price: 2 },
    { name: "cherry", color: "red", price: 3 },
    { name: "strawberry", color: "red", price: 4 },
  ]

  mappingItems = (list) => {
    return list.map((item)=>{return <li>{item.name}</li>})
  }

  redFruits = this.fruits.filter((item)=>{
    return item.color === "red" 
  })

  totalPrice = this.fruits.reduce((previousPrice, currentItem)=>{
    return currentItem.price + previousPrice
  }, 0) 

  
  render() {
    return (
      <div className="App" >
        <div id="all-fruit-names"><Fruit title="All Fruits" items={this.mappingItems(this.fruits)}></Fruit></div>
        <div id="red-fruit-names"><Fruit title="Red Fruits" items={this.mappingItems(this.redFruits)}></Fruit></div>
        <div id="total"><Fruit title="Total Price" items={<li>{this.totalPrice}</li>}/></div>
      </div>
    )
  }
}

export default App;
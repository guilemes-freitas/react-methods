import { Component } from 'react';
import './style.css'
class Fruit extends Component{
    render(){
        return(
            <>
                <h1 className="title">{this.props.title}</h1>
                <ul className="itemsList">
                    {this.props.items}
                </ul>
            </>
        );
    }
}

export default Fruit